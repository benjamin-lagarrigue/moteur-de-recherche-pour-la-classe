# Moteur de recherche pour la classe
Ce moteur de recherche interroge exclusivement des sites institutionnels pour optimiser l'efficacité des recherches.
Il a été conçu pour : 
- aider les enseignants à préparer leur classe 
- permettre aux formateurs de concevoir une action de formation ou  conseiller un enseignant lors d'une visite. 
- se former

## Remarque sur le fonctionnement
Après une recherche, vous pouvez revenir à la page d'accueil en cliquant sur le bout "précédent" ou bien modifier votre mot clé en prenant garde à bien **conserver tout ce qui se trouve entre parenthèses**. Il s'agit du filtre : l'ensemble des sites interrogés. 

# CSE : Custom Searx Engine
Le moteur de recherche pour la classe est une adaptation d'Un outil créé par Cédric Eyssette, qui permet de créer un moteur de recherche personnalisé à partir d'une liste de sites

1. Créez un fichier sur CodiMD ou sur une forge.
2. Ce fichier doit comporter : un titre, un bloc de citation qui constituera le message initial, et une liste de sites. Vous pouvez récupérer [ce modèle](https://codimd.apps.education.fr/b8KAltV2QQWR2rKhF_eYcg?both) ou bien regarder cet [exemple](https://cse.forge.apps.education.fr/#https://eyssette.forge.apps.education.fr/my-cse/intro-philo.md).
3. Votre moteur de recherche sera alors disponible à l'adresse : https://cse.forge.apps.education.fr/#URL (en remplaçant URL par l'URL de votre fichier).

## Forker le projet

Si vous forkez le projet, vous pouvez changer le moteur de recherche par défaut, préciser la langue de recherche, et utiliser des raccourcis pour pouvoir partager votre moteur de recherche avec une URL plus courte.

## Crédits

_Custom Searx Engine_ est un outil libre et gratuit sous licence MIT.

Il utilise d'autres logiciels libres :
- [showdown](https://github.com/showdownjs/showdown) pour la conversion du markdown en html
- [SearXNG](https://github.com/searxng/searxng) comme moteur de recherche ; par défaut l'instance Searx est celle de [Zaclys](https://www.zaclys.com/) : [https://zotop.zaclys.com/](https://zotop.zaclys.com/)

## Remerciements
Je tiens à remercier particulièrement Cédric Eyssette pour son outil original et Arnaud Champollion pour l'aide apportée pour réaliser ce moteur de recherche pour la classe.