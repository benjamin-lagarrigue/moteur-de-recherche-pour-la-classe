# Moteur de recherche pour la classe

> :mag_right: Ce moteur de recherche interroge : <br /> <br />
> **Les sites institutionnels pour préparer la classe**<br />
> :ballot_box_with_check: Eduscol <br />
> :ballot_box_with_check: Education.gouv <br />
> :ballot_box_with_check: Primàbord <br />
> :ballot_box_with_check: Edubase <br />
> :ballot_box_with_check: Toutes les académies de France métropolitaine et d'outre-mer<br /><br />
> 
> **Les sites pour se former** <br />
> :ballot_box_with_check: Ifé <br />
> :ballot_box_with_check: CNESCO <br />
> :ballot_box_with_check: M@gistère <br />
> :ballot_box_with_check: Canope + Cap école inclusive + Canotech <br />
> :ballot_box_with_check: Cairn, HAL, Gallica
> <br /><br />

----

- https://www.education.gouv.fr/
- https://ife.ens-lyon.fr/presentation/linstitut-francais-de-leducation
- https://www.reseau-canope.fr/
- https://eduscol.education.fr/
- https://primabord.eduscol.education.fr/
- https://edubase.eduscol.education.fr/
- https://www.cnesco.fr/
- https://magistere.education.fr/
- https://www.reseau-canope.fr/cap-ecole-inclusive.html
- https://canotech.fr
- https://www.cairn.info/
- https://hal.science/
- https://gallica.bnf.fr/accueil/fr/content/accueil-fr?mode=desktop
- ac-toulouse.fr
- ac-montpellier.fr
- ac-Lyon.fr
- ac-nice.fr
- ac-aix-marseille.fr
- ac-corse.fr
- ac-bordeaux.fr
- ac-limoges.fr
- ac-clermont-ferrand.fr
- ac-poitiers.fr
- ac-grenoble.fr
- ac-besançon.fr
- ac-dijon.fr
- ac-orleans-tours.fr
- ac-nantes.fr
- ac-rennes.fr
- ac-versailles.fr
- ac-paris.fr
- ac-creteil.fr
- ac-reims.fr
- ac-nancy-metz.fr
- ac-strasbourg.fr
- ac-amiens.fr
- ac-lille.fr
- ac-besançon.fr
- ac-normandie.fr
- ac-guadeloupe.fr
- ac-reunion.fr
- ac-guyane.fr
- ac-martinique.fr
- ac-mayotte.fr
